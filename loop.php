<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="entry">
        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

        <?php the_content('Czytaj dalej...'); ?>
        <br class="clear" />
    </div> <!-- end entry -->

    <div class="entry-author">Dodał <span><?php the_author(); ?></span></div>
    <div class="entry-date">&nbsp;w dniu <span><?php the_time('d-m-Y'); ?></span></div>
    <div class="postmetadata">Opublikowano w kategorii <?php the_category(', '); ?></div>

<?php endwhile; else: ?>
    <p><?php _e('Nie znaleziono postów spełniających podane kryteria.'); ?></p>
<?php endif; ?>