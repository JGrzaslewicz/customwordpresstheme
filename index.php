<?php
/**
 * The main template file.
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php
                get_template_part( 'loop' );
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();